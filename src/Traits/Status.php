<?php

namespace PanelSsh\Core\Traits;

use Illuminate\Database\Eloquent\Builder;

/**
 * @method static $this|Builder active()
 * @method static $this|Builder inactive()
 */
trait Status
{
    public function scopeActive(Builder $query)
    {
        return $query->where('is_active', true);
    }

    public function scopeInactive(Builder $query)
    {
        return $query->where('is_active', false);
    }
}
