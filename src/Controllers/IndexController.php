<?php

namespace PanelSsh\Core\Controllers;

use Illuminate\Database\Eloquent\Model;
use PanelSsh\Core\DataTables\DataTable;

/**
 * @property DataTable $dataTable
 * @property Model     $dataTableModel
 * @property array     $dataTableParams
 * @property string    $indexLayout
 */
trait IndexController
{
    public function index()
    {
        return (new $this->dataTable())
            ->with($this->dataTableParams())
            ->setModel($this->dataTableModel())
            ->setRoute($this->route())
            ->render($this->indexLayout(), $this->vars());
    }

    protected function dataTable()
    {
        if (property_exists($this, 'dataTable')) {
            return $this->dataTable;
        }

        return $this->getDataTable();
    }

    protected function getDataTable()
    {
        return $this->dataTable;
    }

    protected function setDataTable(DataTable $dataTable)
    {
        $this->dataTable = $dataTable;

        return $this;
    }

    protected function dataTableModel()
    {
        if (property_exists($this, 'dataTableModel')) {
            return $this->dataTableModel;
        }

        return $this->getDataTableModel();
    }

    protected function getDataTableModel()
    {
        return $this->dataTableModel ?? $this->model();
    }

    protected function setDataTableModel(Model $dataTableModel)
    {
        $this->dataTableModel = $dataTableModel;

        return $this;
    }

    protected function dataTableParams()
    {
        if (property_exists($this, 'dataTableParams')) {
            return $this->dataTableParams;
        }

        return $this->getDataTableParams();
    }

    protected function getDataTableParams()
    {
        return $this->dataTableParams ?? [];
    }

    protected function setDataTableParams(array $dataTableParams)
    {
        $this->dataTableParams = $dataTableParams;

        return $this;
    }

    protected function indexLayout()
    {
        return $this->getIndexLayout() ?: 'dashboard.pages.index';
    }

    protected function getIndexLayout()
    {
        if (property_exists($this, 'indexLayout')) {
            return $this->indexLayout;
        }

        return null;
    }

    protected function setIndexLayout(string $indexLayout)
    {
        $this->indexLayout = $indexLayout;

        return $this;
    }
}
