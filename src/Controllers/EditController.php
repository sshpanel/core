<?php

namespace PanelSsh\Core\Controllers;

/**
 * @property string $editLayout
 */
trait EditController
{
    public function edit()
    {
        $title = data_get($this->titleAlt(), 'edit', $this->title());

        return $this
            ->addVars([
                'forms' => $this->editForms($this->data()),
                'form_title' => $title,
                'form_method' => 'put',
                'form_action' => route("{$this->route}.update", $this->parameters()),
                'button_reset' => true,
                'button_update' => true,
            ])
            ->appendBreadcrumbs(['title' => $title])
            ->editRender();
    }

    protected function editForms($data)
    {
        return [];
    }

    protected function editLayout()
    {
        return $this->getEditLayout() ?: 'dashboard.pages.edit';
    }

    protected function getEditLayout()
    {
        if (property_exists($this, 'editLayout')) {
            return $this->editLayout;
        }

        return null;
    }

    protected function setEditLayout(string $editLayout)
    {
        $this->editLayout = $editLayout;

        return $this;
    }

    protected function editRender()
    {
        return view($this->editLayout(), $this->vars());
    }
}
