<?php

namespace PanelSsh\Core\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

trait DestroyController
{
    public function destroy(Request $request)
    {
        abort_unless($request->ajax(), 404);

        /** @var $data \Illuminate\Database\Eloquent\Model */
        $data = $this->data();

        DB::beginTransaction();
        try {
            $this->performBeforeDestroy($request, $data);

            $data->delete();

            $this->performAfterDestroy($request, $data);

            DB::commit();
        } catch (\Exception $e) {
            report($e);

            DB::rollBack();

            return response()->json(['message' => $e->getMessage()], 500);
        }

        return response()->json([
            'status' => 'ok',
            'message' => __('dashboard.destroy.success', ['title' => Str::singular($this->title())]),
        ]);
    }

    protected function performBeforeDestroy($request, $model)
    {
        //$model->modelRelation()->delete();
    }

    protected function performAfterDestroy($request, $model)
    {
    }
}
