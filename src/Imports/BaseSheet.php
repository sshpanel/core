<?php

namespace PanelSsh\Core\Imports;

use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Database\Schema\Builder as SchemaBuilder;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\SkipsUnknownSheets;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Concerns\WithValidation;

abstract class BaseSheet implements ToModel, WithHeadingRow, WithValidation, SkipsUnknownSheets
{
    use Importable;

    public $query;

    public $attribute;

    abstract public function key(): string;

    abstract public function val(): string;

    abstract public function rules(): array;

    public function onUnknownSheet($sheetName)
    {
        info("Sheet {$sheetName} was skipped");
    }

    public function getQuery()
    {
        if ($this->query instanceof EloquentBuilder ||
            $this->query instanceof QueryBuilder ||
            $this->query instanceof SchemaBuilder) {
            return $this->query;
        }

        return (new $this->query)->newQuery();
    }

    public function setQuery($query)
    {
        $this->query = $query;

        return $this;
    }

    public function setAttribute($attribute)
    {
        $this->attribute = $attribute;

        return $this;
    }

    public function model(array $row)
    {
        $data = $this->getQuery()->select(['id', $this->key(), $this->attribute])->where($this->key(), $row[$this->val()])->first();

        if (blank($data)) {
            throw (new ModelNotFoundException)->setModel(get_class($this->query), $row[$this->val()]);
        }

        $data->update([$this->attribute => collect($data->{$this->attribute})->add($row)->toArray()]);
    }
}
