<?php

namespace PanelSsh\Core\Controllers;

use Illuminate\Http\Request;
use PanelSsh\Core\Imports\MultipleImport;

/**
 * @property string $excelImporter
 */
trait ImportController
{
    public function import(Request $request)
    {
        if ($this->excelImporter()) {
            $request->validate(['file' => ['required']]);

            \DB::beginTransaction();
            try {
                if (is_array($this->excelImporter())) {
                    (new MultipleImport)->setQuery($this->model())->setImports($this->excelImporter())->import($request->file('file'));
                } else {
                    (new $this->excelImporter())->setQuery($this->model())->import($request->file('file'));
                }

                \DB::commit();
            } catch (\Exception $e) {
                report($e);

                \DB::rollBack();

                throw_if(config('app.debug'), $e);

                return response()->json(['message' => $e->getMessage()], 500);
            }

            return response()->json([
                'status' => 'ok',
                'message' => __('dashboard.import.success', ['title' => $this->title()]),
            ]);
        }

        return response()->json(['message' => __('dashboard.import.error')], 403);
    }

    protected function excelImporter()
    {
        return $this->getExcelImporter();
    }

    protected function getExcelImporter()
    {
        return $this->excelImporter ?? null;
    }

    protected function setExcelImporter(string $excelImporter)
    {
        $this->excelImporter = $excelImporter;

        return $this;
    }
}
