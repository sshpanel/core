<?php

namespace PanelSsh\Core\Controllers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

/**
 * @property string $statusColumn
 */
trait StatusController
{
    public function status(Request $request)
    {
        abort_unless($request->ajax(), 404);

        /** @var $data Model */
        $data = $this->data();

        DB::beginTransaction();
        try {
            $this->performBeforeStatus($request, $data);

            $data->update([$this->statusActiveColumn() => !$data->{$this->statusColumn()}]);

            $this->performAfterStatus($request, $data);

            DB::commit();
        } catch (\Exception $e) {
            report($e);

            DB::rollBack();

            return response()->json(['message' => $e->getMessage()], 500);
        }

        return response()->json([
            'status' => 'ok',
            'message' => __('dashboard.status.success', ['title' => Str::singular(strtolower($this->title()))]),
        ]);
    }

    protected function statusColumn()
    {
        if (property_exists($this, 'statusColumn')) {
            return $this->statusColumn;
        }

        return $this->getStatusColumn();
    }

    protected function getStatusColumn()
    {
        return $this->statusColumn ?? 'is_active';
    }

    protected function setStatusColumn(string $statusColumn)
    {
        $this->statusColumn = $statusColumn;

        return $this;
    }

    protected function performBeforeStatus($request, $model)
    {
    }

    protected function performAfterStatus($request, $model)
    {
    }
}
