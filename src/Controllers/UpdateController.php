<?php

namespace PanelSsh\Core\Controllers;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\DB;

/**
 * @property FormRequest $updateFormRequest
 */
trait UpdateController
{
    public function update()
    {
        $request = $this->updateFormRequest();

        $data = $this->data();

        DB::beginTransaction();
        try {
            $this->performBeforeUpdate($request, $data);

            $this->performUpdate($request, $data);

            $this->performAfterUpdate($request, $data);

            DB::commit();
        } catch (\Exception $e) {
            report($e);

            DB::rollBack();

            throw_if(config('app.debug'), $e);

            return redirect()->back()->withInput($request->input());
        }

        return redirect()->route("{$this->route}.index");
    }

    public function updateFormRequest()
    {
        if (property_exists($this, 'updateFormRequest')) {
            return app($this->updateFormRequest);
        }

        return app('request');
    }

    public function getUpdateFormRequest()
    {
        return $this->updateFormRequest;
    }

    public function setUpdateFormRequest($updateFormRequest)
    {
        $this->updateFormRequest = $updateFormRequest;

        return $this;
    }

    protected function performBeforeUpdate($request, $model)
    {
    }

    protected function performUpdate($request, $model)
    {
        return $model->update($request->input());
    }

    protected function performAfterUpdate($request, $model)
    {
    }
}
