<?php

namespace PanelSsh\Core\Imports;

use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class MultipleImport implements WithMultipleSheets
{
    use Importable;

    public $query;

    public $imports;

    public function sheets(): array
    {
        $sheets = [];

        foreach ($this->imports as $class => $import) {
            $sheetClass = new $class;

            if (isset($import)) {
                $sheets[] = $sheetClass->setQuery($this->query)->setAttribute($import);
            } else {
                $sheets[] = $sheetClass->setQuery($this->query);
            }
        }

        return $sheets;
    }

    public function setQuery($query)
    {
        $this->query = $query;

        return $this;
    }

    public function setImports($imports)
    {
        $this->imports = $imports;

        return $this;
    }
}
