<?php

namespace PanelSsh\Core\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

/**
 * @property string $destroyAllIdentifier
 */
trait DestroyAllController
{
    public function destroyAll(Request $request)
    {
        $request->validate([
            'ids' => ['required', 'array'],
        ]);

        DB::beginTransaction();
        try {
            $this->performBeforeDestroyAll($request);

            $this->performDestroyAll($request);

            $this->performAfterDestroyAll($request);

            DB::commit();
        } catch (\Exception $e) {
            report($e);

            DB::rollBack();

            throw_if(config('app.debug'), $e);

            return response()->json(['message' => $e->getMessage()], 500);
        }

        return response()->json([
            'status' => 'ok',
            'message' => __('dashboard.destroyAll.success', ['title' => $this->title()]),
        ]);
    }

    protected function destroyAllIdentifier()
    {
        if (property_exists($this, 'destroyAllIdentifier')) {
            return $this->destroyAllIdentifier;
        }

        return $this->getDestroyAllIdentifier();
    }

    protected function getDestroyAllIdentifier()
    {
        return $this->destroyAllIdentifier ?? 'id_ext';
    }

    protected function setDestroyAllIdentifier(string $destroyAllIdentifier)
    {
        $this->destroyAllIdentifier = $destroyAllIdentifier;

        return $this;
    }

    protected function performBeforeDestroyAll($request)
    {
        //ModelRelation::query()->whereIn($this->destroyAllIdentifier(), $request->ids)->delete();
    }

    protected function performDestroyAll($request)
    {
        $this->model()->whereIn($this->destroyAllIdentifier(), $request->ids)->delete();
    }

    protected function performAfterDestroyAll($request)
    {
        //ModelRelation::query()->whereIn($this->destroyAllIdentifier(), $request->ids)->delete();
    }
}
