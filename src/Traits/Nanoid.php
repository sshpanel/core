<?php

namespace PanelSsh\Core\Traits;

/**
 * @property int $id_ext
 */
trait Nanoid
{
    protected static function bootNanoid()
    {
        static::creating(function ($model) {
            $model->id_ext = nanoid();
        });
    }
}
