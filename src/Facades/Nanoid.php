<?php

namespace PanelSsh\Core\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @method static string generate()
 */
class Nanoid extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'nanoid';
    }

}
